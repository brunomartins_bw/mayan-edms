��    (      \  5   �      p  9   q     �     �     �     �     �     
               &      /  ;   P  7   �     �     �     �     �     �               1     B  B   T  :   �  6   �  9   	  	   C     M     R     c     q     w     �  *   �     �     �     �             {  #  Q   �     �  :   	  7   =	  *   u	  5   �	  $   �	     �	     
     
  ;    
  i   \
  d   �
  %   +  %   Q     w  !   �  -   �     �  -   �          ?  f   _  V   �  \     Z   z     �     �  +   �  '   '     O  "   ^     �  X   �  "   �       7      &   X  !        %         &                                              "   (                                    $      	               !   #                     '                     
           %(model_name)s with this %(field_labels)s already exists. Add Add document "%s" to cabinets Add documents to cabinets Add new level Add new level to: %s Add to cabinets All Cabinet Cabinets Cabinets containing document: %s Cabinets from which the selected documents will be removed. Cabinets to which the selected documents will be added. Create cabinet Create cabinets Delete Delete cabinets Delete the cabinet: %s? Details Details of cabinet: %s Document cabinet Document cabinets Document: %(document)s added to cabinet: %(cabinet)s successfully. Document: %(document)s is already in cabinet: %(cabinet)s. Document: %(document)s is not in cabinet: %(cabinet)s. Document: %(document)s removed from cabinet: %(cabinet)s. Documents Edit Edit cabinet: %s Edit cabinets Label List of children cabinets. Navigation: Number of documents on this cabinet level. Parent and Label Remove Remove documents from cabinets Remove from cabinets View cabinets Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-04-21 16:25+0000
Last-Translator: Hmayag Antonian <hmayag@freemail.gr>, 2018
Language-Team: Greek (https://www.transifex.com/rosarior/teams/13584/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 %(model_name)s με την ετικέτα %(field_labels)s υπάρχει ήδη. Προσθήκη Προσθήκη "%s" εγγράφου σε ερμάρια Προσθήκη εγγράφων στα ερμάρια Προσθήκη νέου επιπέδου Προσθήκη νέου επιπέδου στο: %s Προθήκη στα ερμάρια Όλα Ερμάριο Ερμάρια Ερμάρια που περιέχουν έγγραφο: %s Ερμάρια από τα οποία τα επιλεγμένα έγγραφα θα αφαιρεθούν. Ερμάρια στα οποία τα επιλεγμένα έγγραφα θα προστεθούν. Δημιουργία ερμαρίου Δημιουργία ερμαρίων Διαγραφή Διαγραφή ερμαρίων Διαγραφή του ερμαρίου: %s? Λεπτομέρειες Λεπτομέρειες ερμαρίου: %s Ερμάριο εγγράφων Ερμάρια εγγράφων Έγγραφο: %(document)s προστέθηκε στο ερμάριο: %(cabinet)s επιτυχώς. Έγγραφο: %(document)s υπάρχει ήδη στο ερμάριο: %(cabinet)s. Έγγραφο: %(document)s δεν περιέχεται στο ερμάριο: %(cabinet)s. Έγγραφο: %(document)s αφαιρέθηκε από το ερμάριο: %(cabinet)s. Έγγραφα Τροποποίηση Τροποποίηση ερμαρίου: %s Τροποποίηση ερμαρίων Ετικέτα Λίστα υπο-ερμαρίων Πλοήγηση: Αριθμός εγγράφων σε αυτό το επίπεδο του ερμάριο. Γονέας και ετικέτα Αφαίρεση Αφαίρεση εγγράφων από ερμάρια Αφαίρεση από ερμάρια Εμφάνηση ερμαρίων 